<?php
require_once('animal.php');
require_once('ape.php');
require_once('frog.php');
$sheep = new Animal("shaun");


// release 0
echo $sheep->name; // "shaun"
echo $sheep->legs; // 4
echo $sheep->cold_blooded; // "no"


echo'<br><br>';
// release 1
$sungokong = new Ape("kera sakti");
$sungokong->yell(); // "Auooo"
echo $sungokong-> name . '<br>'; // "shaun"
echo $sungokong-> legs . '<br>'; // 4
echo $sungokong-> cold_blooded . '<br>'; // "no"

$kodok = new Frog("buduk");
$kodok->jump() ; // "hop hop"
echo $kodok-> name . '<br>'; // "shaun"
echo $kodok-> legs . '<br>'; // 4
echo $kodok-> cold_blooded . '<br>'; // "no"
?>