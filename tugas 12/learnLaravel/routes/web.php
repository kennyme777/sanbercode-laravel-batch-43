<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::get('/',[HomeController::class,'dashboard']);
Route::get('/register', [AuthController::class, 'register']);
Route::get('/welcome', [AuthController::class, 'welcome']);
Route::post('/send', [AuthController::class, 'send']);

Route::get('/table', [HomeController::class, 'table']);
Route::get('/data-table', [HomeController::class, 'dataTable']);


// Cast
Route::get('/cast', [CastController::class, 'index']);
Route::get('/cast/{id}', [CastController::class, 'show']);

Route::get('/cast/create', [CastController::class, 'create']);
Route::post('/cast', [CastController::class, 'store']);

Route::get('/cast/{id}/edit',  [CastController::class, 'edit']);
Route::put('/cast/{id}', [CastController::class, 'update']);

Route::delete('/cast/{id}', [CastController::class, 'destroy']);

