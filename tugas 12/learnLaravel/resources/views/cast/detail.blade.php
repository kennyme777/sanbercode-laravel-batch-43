@extends('layouts.master')

@section('title')
    Detail
@endsection

@section('sub-title')
    Detail
@endsection


@section('content')
<h1>{{ $cast->name }}</h1>
<h4>{{ $cast->email }}</h4>
<p>{{ $cast->bio }}</p>
@endsection
