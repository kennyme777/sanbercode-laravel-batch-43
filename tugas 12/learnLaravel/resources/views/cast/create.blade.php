@extends('layouts.master')

@section('title')
    Table
@endsection

@section('sub-title')
    Table
@endsection


@section('content')
    <form action="/cast" method="post">
        @csrf
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" id="name" name="name">
            @error('name')
                <div class="invalid-feedback">
                    Required
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input type="text" class="form-control" id="email" name="email">
            @error('email')
                <div class="invalid-feedback">
                    Required
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="bio">Bio</label>
            <input type="text" class="form-control" id="bio" name="bio">
            @error('bio')
                <div class="invalid-feedback">
                    Required
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
