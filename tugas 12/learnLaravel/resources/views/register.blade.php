<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>

    <h3>Sign Up Form</h3>
    <form action="/send" method="post">
        @csrf

        <p>
            <label for="first_name">First name:</label><br>
            <input id="first_name" name="first_name" type="text">
        </p>

        <p>
            <label for="last_name">Last name:</label><br>
            <input id="last_name" name="last_name" type="text">
        </p>

        <p>Gender: <br>
            <input type="radio" id="male" name="gender" value="male">
            <label for="male">Male</label><br>
            <input type="radio" id="female" name="gender" value="female">
            <label for="female">Female</label><br>
            <input type="radio" id="other" name="gender" value="other">
            <label for="other">Other</label>
        </p>

        <p>Nationality: <br>
            <select name="nationality" id="nationality">
                <option value="indonesian">Indonesian</option>
            </select>
        </p>

        <p>Language spoken: <br>
            <input type="checkbox" id="language1" name="language1" value="Bahasa Indonesia">
            <label for="language1">Bahasa Indonesia</label><br>
            <input type="checkbox" id="language2" name="language2" value="English">
            <label for="language2">English</label><br>
            <input type="checkbox" id="language3" name="language3" value="Other">
            <label for="language3">Other</label><br>
        </p>

        <p>Bio: <br>
            <textarea name="Bio" id="Bio" cols="30" rows="10"></textarea>
        </p>

        <button type="submit">Sign Up</button>

    </form>
</body>

</html>