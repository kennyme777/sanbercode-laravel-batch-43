<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function dashboard(){
        return view('welcome');
    }

    public function table()
    {
        return view('pages.table');
    }

    public function dataTable()
    {
        return view('pages.data-table');
    }
}
