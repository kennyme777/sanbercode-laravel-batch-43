<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{

    public function index()
    {
        $casts = DB::table('casts')->get();
        return view('cast.index', [
            'casts' => $casts
        ]);
    }


    public function create()
    {
        return view('cast.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => '',
            'bio' => ''
        ]);

        DB::table('casts')->insert([
            'name' => $request['name'],
            'email' => $request['email'],
            'bio' => $request['bio'],
        ]);

        return redirect('/cast');
    }

    public function show($id)
    {
        $cast = DB::table('casts')->find($id);
        return view('cast.detail', [
            'cast' => $cast
        ]);
    }

    public function edit($id)
    {
        $cast = DB::table('casts')->find($id);
        return view('cast.edit', [
            'cast' => $cast
        ]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'email' => '',
            'bio' => ''
        ]);

        DB::table('casts')
            ->where('id', $id)
            ->update([
                'name' => $request['name'],
                'email' => $request['email'],
                'bio' => $request['bio'],
            ]);

        return redirect('/cast');
    }

    public function destroy($id){
        DB::table('casts')->where('id', '=', $id)->delete();
        
        return redirect('cast');
    }
}
